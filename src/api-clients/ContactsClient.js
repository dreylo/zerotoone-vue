import {ApiClientFactory} from '@/ApiClientFactory';

let clientFactory = new ApiClientFactory();

export const ContactsClient = {

	/**
	* Gets and returns a user's contacts.
	*/
	all() {
		return clientFactory.get('/contacts');
	},

	/**
	* Sends a request to create new contact.
	*/
	create(data) {
		return clientFactory.post('/contacts', data);
	},

	/**
	* Sends a request to update an existing contact.
	*/
	update(id, data) {
		return clientFactory.put(`/contacts/${id}`, data);
	},

	/**
	* Deletes a contact.
	*/
	delete(id) {
		return clientFactory.delete(`/contacts/${id}`);
	},

	/**
	* Retrieves a contact.
	*/
	get(id) {
		return clientFactory.get(`/contacts/${id}`);
	}

}