import {ApiClientFactory} from '@/ApiClientFactory';

let clientFactory = new ApiClientFactory();

export const AuthClient = {

	/**
	* Checks if user is authenticated or not.
	*/
	check() {
		return clientFactory.get('/auth/check');
	},

	/**
	* Logs a user in.
	*/
	login(data) {
		return clientFactory.post('/auth/login', data);
	}

}