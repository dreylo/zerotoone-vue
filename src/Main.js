import Vue from 'vue';
import Router from './Router';
import {Store} from './Store';
import VueCookie from 'vue-cookie';
import Loader from './components/Loader';
import Sidebar from './components/Sidebar';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import {beforeRoute} from './middlewares/AuthMiddleware';
import {AuthClient} from '@/api-clients/AuthClient';

Vue.use(VueCookie);
Vue.use(VueSweetalert2);

window.jQuery = window.$ = require('jquery');

new Vue({
	el: '.app-zcirfeu52g',
	store: Store,
	router: Router,
	components: {
		Loader,
		Sidebar
	},
	mounted() {
		AuthClient.check().then((response) => {
			Store.dispatch('auth/updateAuthenticatedStatus', true);
			Store.dispatch('auth/updateHandshakeStatus', true);
		}).catch((error) => {
			Store.dispatch('auth/updateAuthenticatedStatus', false);
			Store.dispatch('auth/updateHandshakeStatus', true);
		});
	}
});