import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home';
import Profile from './views/Profile';
import Login from './views/auth/Login';
import Register from './views/auth/Register';
import Contacts_Add from './views/Contacts/Add';
import Contacts_Edit from './views/Contacts/Edit';
import Contacts_Index from './views/Contacts/Index';

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{
			name: 'home',
			path: '/',
			component: Home
		},
		{
			name: 'auth.login',
			path: '/login',
			component: Login
		},
		{
			name: 'auth.register',
			path: '/register',
			component: Register
		},
		{
			name: 'contacts.index',
			path: '/contacts',
			component: Contacts_Index
		},
		{
			name: 'contacts.add',
			path: '/contacts/add',
			component: Contacts_Add
		},
		{
			name: 'contacts.edit',
			path: '/contacts/:id',
			component: Contacts_Edit,
			props: true
		},
		{
			name: 'profile',
			path: '/profile',
			component: Profile
		}
	]
});