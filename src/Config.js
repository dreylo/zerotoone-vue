import VueCookie from 'vue-cookie';

export const ACCESS_TOKEN = VueCookie.get('access_token');
export const API_URL = 'http://api.zerotoone.site/v1/';
export const AUTH_ROUTES = [
	'auth.login', 'auth.register'
];
export const AUTH_DEFAULT_ROUTE = '/';